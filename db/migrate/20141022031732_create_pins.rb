class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.float :longitude
      t.float :latitude
      t.integer :score
      t.string :tags
      t.timestamps
    end
  end
end
