class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :username
      t.string :filedir
      t.float :longitude
      t.float :latitude
      t.integer :score
      t.string :caption
      t.string :tags
      t.belongs_to :user
      t.belongs_to :pin
      t.timestamps
    end
  end
end
