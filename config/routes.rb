Rails.application.routes.draw do
  #resources :users

  #user requests
  post "users/add", to:"users#add"
  post "users/login", to: "users#login"
  post "users/addEmail", to: "users#addEmail"
  post "users/delete", to: "users#warmupcleartable"

  post "users/upload", to: "photos#upload"
  
  #photo requests
  post "photos/delete", to: "photos#deleteAllPicture"
  get "photos/getPhotos", to: "photos#getPhotos"
  post "photos/vote", to: "photos#vote"
  # post "photos/downvote", to: "photos#downvote"
  get "photos/getScore", to: "photos#getScore"
  get "photos/getCaption", to: "photos#getCaption"
  get "photos/getHot", to: "photos#getHot"

  #pin requests
  get "pins/getPins", to: "pins#getPins"
  get "pins/getScore", to: "pins#getScore"

  post "/TESTAPI/unitTests", to: "users#warmupstarttests"
  post "/TESTAPI/resetFixture", to: "users#warmupcleartable"
  root to: "users#index";

  get "user/new", to: "users#new" , as: :new_user
  get "user.:id", to: "users#show", as: :user
  post "users", to:"users#create"
  get "users", to:"users#index"
  get 'edit_usersmodel.:id', to: "users#edit", as: :edit_user
  patch "user.:id", to: "users#update"

  resources :sessions

  #get 'user', to: "users#index", as: :user;
  #post 'user', to: "users#edit", as: :edit_usersmodel;

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
