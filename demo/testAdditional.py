"""
Each file that starts with test... in this directory is scanned for subclasses of unittest.TestCase or testLib.RestTestCase
"""

import unittest
import os
import testLib
import json



class TestDemo(testLib.RestTestCase):
    """
    Test Server Returning Error Message
    """
    def testAddDemoData(self):
		self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon1', 'password' : 'beacon1', 'count' : 0 })

		self.sendFileRequest("/users/upload",file="james1.png", data = {'longitude' : 37.879883, 'latitude' :-122.269478, 'caption' : "cheeseboard!!!", 'tags' : "#cheeseboard"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="james2.png", data = {'longitude' : 37.846282, 'latitude' :-122.252115, 'caption' : "zachary\'s pizza", 'tags' : "#pizza"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="james3.png", data = {'longitude' : 37.872035, 'latitude' :-122.257838, 'caption' : "berkeley campanile", 'tags' : "#campanile"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="james4.png", data = {'longitude' : 37.875710, 'latitude' :-122.259027, 'caption' : "soda hall", 'tags' : "#soda"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="james5.png", data = {'longitude' : 37.869525, 'latitude' :-122.258842, 'caption' : "uc berkeley", 'tags' : "#berkeley"}, thecookies = {'session_id' : 'beacon1'})

		self.sendFileRequest("/users/upload",file="josh1.png", data = {'longitude' : 37.867487, 'latitude' :-122.258925, 'caption' : "Pappy\'s Bar", 'tags' : "#bar"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="josh2.png", data = {'longitude' : 37.867936, 'latitude' :-122.259605, 'caption' : "Kip\'s", 'tags' : "#bar"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="josh3.png", data = {'longitude' : 37.873411, 'latitude' :-122.268770, 'caption' : "Triple Rock", 'tags' : "#bar"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="josh4.png", data = {'longitude' : 37.867988, 'latitude' :-122.258327, 'caption' : "Senores!!!", 'tags' : "#pizza"}, thecookies = {'session_id' : 'beacon1'})
		self.sendFileRequest("/users/upload",file="josh5.png", data = {'longitude' : 37.875190, 'latitude' :-122.259775, 'caption' : "Pizahhh yum", 'tags' : "#pizza"}, thecookies = {'session_id' : 'beacon1'})
		self.assertEquals(True, True)





        








   






