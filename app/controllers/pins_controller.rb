class PinsController < ApplicationController
  skip_before_action :verify_authenticity_token

	def getPins
		result = Pin.getPins(params[:lat], params[:lon], params[:rad])
		render :json => { :pins => result }
 	end

 	def getScore
 		result = Pin.getScore(params[:pinID])
 		render :json => { :score => result }
 	end
end