class UsersController < ApplicationController
  protect_from_forgery
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_filter  :verify_authenticity_token
  
  # GET /user
  # GET /user.json
  def index
    @user = User.all
  end

  # GET /user/1
  # GET /user/1.json
  def show
    @user = User.find(params[:id])
  end

  # GET /user/new
  def new
    @User = User.new
  end

  # GET /user/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /user
  # POST /user.json
  def create
    @User = User.new(user_params)
    respond_to do |format|
      result = @User.add(user_params)
      if result>0
        format.html { redirect_to @User, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @User }
      else
        @User.errors["Error Type"] = result
        format.html {render :new}
        format.json {redner json: @User.errors}
        #format.html { render json: result}
        #format.json { render json: @User.errors, status: :unprocessable_entity }
        #format.json {render json: result}
      end
    end
  end

  def add
    result = User.add(params)
    if (result > 0)
      CookiesController.set_cookies(cookies, params[:username])
      render :json => { :errCode => User::SUCCESS, :count => result }
    else
      render :json => { :errCode => result}
    end
  end

  def login
    result = User.login({:username => params[:username], :password =>params[:password]})
    if (result < 0)
      render :json => {:errCode => result}
    else
      CookiesController.set_cookies(cookies, params[:username])
      render :json => {:errCode => User::SUCCESS, :count => result}
    end
  end

  def addEmail
    result = User.addEmail({:username => cookies["session_id"], :email => params[:email]})
    if (result < 0)
      render :json => {:errCode => result}
    else 
      #gonna change this later!
      render :json => {:errCode => User::SUCCESS, :email => params[:email], :session_id => cookies["session_id"]}
    end
  end

  def warmupcleartable
    CookiesController.delete_cookies(cookies)
    result = User.TESTAPI_resetFixture()
    render :json => { :errCode => User::SUCCESS }
  end

  def warmupstarttests
    system("rake test test/models/usersmodel_test.rb > testresult" )
    result = ""
    f = File.open("testresult", "r")
    f.each_line do |line|
      result+=line
    end
    f.close
    resultsplit = result.split
    runs = resultsplit.index("runs,")-1
    failures = resultsplit.index("failures,")-1
    render :json => { :nrFailed => resultsplit[failures].to_i, :totalTests => resultsplit[runs].to_i, :output => result}
  end

  # PATCH/PUT /user/1
  # PATCH/PUT /user/1.json
  def update
    respond_to do |format|
      if @User.update(user_params)
        format.html { redirect_to @User, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @User }
      else
        format.html { render :edit }
        format.json { render json: @User.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /user/1
  # DELETE /user/1.json
  def destroy
    @User.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @User = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:User).permit(:username, :password, :count, :email)
    end
end