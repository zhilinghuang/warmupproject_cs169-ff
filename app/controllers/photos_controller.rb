require 'base64' #required to get photos and pass them to JSON
class PhotosController < ApplicationController
  skip_before_action :verify_authenticity_token
  SUCCESS = 1
  def upload
    t = Time.now.strftime("%y-%m-%d_%H-%M_")
    fileName = t+params[:file].original_filename
    photo = Photo.add(cookies["session_id"], fileName, params[:latitude], params[:longitude], params[:file], params[:caption], params[:tags])
    render :json => { :session_id => cookies["session_id"], :pinID => photo.pin[:id], :photoID => photo[:id], :message => photo[:filedir], :score => photo[:score], :caption => photo[:caption],:tags => photo[:tags] }
  end

 # def savePicture(file, name)
 #    directory = "images"
 #    path = File.join(directory, name)
 #    File.open(path, "wb") { |f| f.write(file.read) }
 #  end

  def deleteAllPicture()
    Photo.TESTAPI_resetFixture()
    Pin.TESTAPI_resetFixture()
    #exec('rm images/*')
    render :json => { :errCode => SUCCESS }
  end

  def getPhotos
    mydata = Photo.getPhotos(params[:pinID])
    # myfile = File.open(Rails.root.join('tmp').join("tmpPic.png"), "wb")
    # myfile.write(mydata)
    #render :json => { :photos => result.to_json}
    #send_file myfile, type: 'image/png', disposition: 'inline'
    #send_file Rails.root.join('tmp').join("tmpPic.png"), type: "image/gif", disposition: "inline"
    #send_data mydata, type: "image/png", disposition: 'inline'
    #send_data mydata, :type => 'image/png',:disposition => 'inline'
    render :json => mydata
  end

  def vote
    result = Photo.vote(params[:photoID], params[:dv])
    render :json => { :score => result }
  end

  # def downvote
  #   result = Photo.downvote(params[:photoID])
  #   render :json => { :score => result }
  # end

  def getScore
    result = Photo.getScore(params[:photoID])
    render :json => { :score => result }
  end
  
  def getHot
    result = Photo.getHot(params[:photoID])
    render :json => { :hot => result }
  end

  def getCaption
    result = Photo.getCaption(params[:photoID])
    render :json => { :caption => result }
  end
end

