class CookiesController < ApplicationController
  def self.set_cookies(my_cookies, user)
    my_cookies["session_id"]   = { :value => user, :expires => 1.hour.from_now }
  end


  def self.delete_cookies(my_cookies)
    my_cookies.delete :session_id
  end

end