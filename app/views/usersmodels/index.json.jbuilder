json.array!(@usersmodels) do |usersmodel|
  json.extract! usersmodel, :id, :user, :password, :count
  json.url usersmodel_url(usersmodel, format: :json)
end
