class User < ActiveRecord::Base
  SUCCESS = 1
  ERR_BAD_CREDENTIALS = -1
  ERR_USER_EXISTS = -2
  ERR_BAD_USERNAME = -3
  ERR_BAD_PASSWORD = -4
  ERR_EML_EXISTS = -5
  MAX_USERNAME_LENGTH = 20
  MIN_PASSWORD_LENGTH = 6
  MAX_PASSWORD_LENGTH = 128
  has_many :photos
  def self.add(user_params)
    if (user_params[:username] == "" || user_params[:username].length > MAX_USERNAME_LENGTH)
      return ERR_BAD_USERNAME
    elsif (User.find_by(username: user_params[:username]))
      return ERR_USER_EXISTS
    elsif (user_params[:password].length < MIN_PASSWORD_LENGTH || user_params[:password].length > MAX_PASSWORD_LENGTH)
      return ERR_BAD_PASSWORD
    else
      tempparam = user_params.clone
      tempparam[:count] = 1
      newmodel = self.new({:username => user_params[:username], :password => user_params[:password], :count => 1})
      newmodel.save
      return newmodel.count
    end
  end
  def self.login(user_params) 
    if (User.find_by(username: user_params[:username]) == nil || User.find_by(username: user_params[:username])::password != user_params[:password])
      return ERR_BAD_CREDENTIALS
    else
      newparams = {:username => user_params[:username], :password => user_params[:password], :count =>  User.find_by(username: user_params[:username])::count+1}
      User.find_by(username: user_params[:username]).update(newparams)
      return User.find_by(username: user_params[:username])::count
    end
  end
  def self.addEmail(user_params)
    if (User.find_by(username: user_params[:username]) == nil)
      return ERR_BAD_CREDENTIALS
    elsif (User.find_by(email: user_params[:email]))
      return ERR_EML_EXISTS
    else
      newparams = {:email => user_params[:email]}
      User.find_by(username: user_params[:username]).update(newparams)
      return User.find_by(username: user_params[:username])::count
    end
  end
  def self.getID(username)
    return User.find_by(username: username)[:id]
  end

  def self.TESTAPI_resetFixture()
    self.delete_all
    return SUCCESS
  end
end
