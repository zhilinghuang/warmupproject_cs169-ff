class Pin < ActiveRecord::Base 
  acts_as_mappable :default_units => :miles,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude
  SUCCESS = 1
  FAILURE = -1
  has_many :photos
  # will return Pin ID
  def self.add(lat, lon, score,tags)
    newmodel = self.new({:latitude=> lat, :longitude => lon, :score => score, :tags => tags})
    newmodel.save
    return newmodel[:id]
  end

  def self.getPins(lat, lon, rad)
    #we commented the next line because for iteration 1, we want to return all pins instead of actually
    #returning pins within specified range, which the code below already does.
    pins = Pin.within(rad, :origin => [lat, lon])
    processedPins = []
    pins.each { |pin|
      processedPins.push({:id=>pin.id, :longitude=>pin.longitude, :latitude=>pin.latitude, :score=>pin.score, :tags=>pin.tags.split("#")})
    }
    #pins = Pin.all
    return processedPins
  end

  def self.getScore(pinID)
    return Pin.find(pinID)::score
  end 

  def self.TESTAPI_resetFixture()
    self.delete_all
    return SUCCESS
  end
end