require 'aws/s3'
class Photo < ActiveRecord::Base
  acts_as_mappable :default_units => :miles,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude

  AWS_ACCESS_KEY_ID= 'AKIAIKLQWXMT4VLUG35Q'
  AWS_SECRET_ACCESS_KEY= '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'
  S3_BUCKET_NAME= 'zhilinghuangbeacon'
  PHOTO_URL = 'http://s3.amazonaws.com/zhilinghuangbeacon/'
  SUCCESS = 1
  FAILURE = -1
  belongs_to :user
  belongs_to :pin


  def self.add(un, fd, la, lo, fi, cap, tags)
    #within about 50 feet, aggregate
    if (not Pin.within(0.01, :origin => [la, lo]).empty?)
      pinID = Pin.closest(:within => 0.1, :origin => [la, lo])[0][:id]
      newTags = tagsUnion(Pin.find(pinID).tags,tags)
      Pin.find(pinID).update_attribute('tags',newTags)
      newmodel = self.new({:username => un, :filedir => fd, :latitude => la, :longitude => lo, :score => 0, :caption => cap, :tags => tags})
      newmodel.update_attributes(username: User.find_by(username: un))
      newmodel.update_attributes(pin: Pin.find(pinID))
      Photo.uploadToS3(fd,fi)
      newmodel.save
      return newmodel

    #within about 100 feet and tags have to contain one way or the other, aggregate
    elsif (not Pin.within(0.02, :origin => [la, lo]).empty?)
      Pin.within(0.02, :origin => [la, lo]).each { |thePin|
        if (tagsContain(thePin.tags, tags))
          pinID = thePin[:id]
          newTags = tagsUnion(thePin.tags, tags)
          Pin.find(pinID).update_attribute('tags', newTags)
          newmodel = self.new({:username => un, :filedir => fd, :latitude => la, :longitude => lo, :score => 0, :caption => cap, :tags => tags})
          newmodel.update_attributes(username: User.find_by(username: un))
          newmodel.update_attributes(pin: thePin)
          Photo.uploadToS3(fd, fi)
          newmodel.save
          return newmodel
        end
      }
    end
    #If the previous conditions fail, no aggregation
    pinID = Pin.add(la, lo, 0, tags)
    newmodel = self.new({:username => un, :filedir => fd, :latitude => la, :longitude => lo, :score => 0, :caption => cap, :tags => tags})
    newmodel.update_attributes(user: User.find_by(username: un))
    newmodel.update_attributes(pin: Pin.find(pinID))
    Photo.uploadToS3(fd,fi)
    newmodel.save
    return newmodel
  end



  def self.tagsContain(tag1, tag2)
    tagsArray1 = tag1.split("#")
    tagsArray2 = tag2.split("#")

    tag1Intag2 = true
    tagsArray1.each { |tag|
      if (not tagsArray2.include?(tag))
        tag1Intag2 = false
        break
      end
    }

    tag2Intag1 = true
    tagsArray2.each { |tag|
      if (not tagsArray1.include?(tag))
        tag2Intag1 = false
        break
      end
    }
    return tag1Intag2 || tag2Intag1
  end

  def self.tagsUnion(tag1,tag2)
    tagsArray1 = tag1.split("#")
    tagsArray2 = tag2.split("#")
    newArray = []
    newString = ""
    tagsArray1.each { |tag|
      if (not newArray.include?(tag))
        newArray.append(tag)
        newString+=(tag+"#")
      end
    }
    tagsArray2.each { |tag|
      if (not newArray.include?(tag))
        newArray.append(tag)
        newString+=(tag+"#")
      end
    }

    return newString
  end




  def self.uploadToS3(filename, myPic)
    AWS.config(access_key_id: AWS_ACCESS_KEY_ID, secret_access_key: AWS_SECRET_ACCESS_KEY)
    s3 = AWS::S3.new
    s3.buckets[S3_BUCKET_NAME].objects[filename].write(:file => myPic.path())
  end

  def self.getPhotos(pinID)
    result = []
    Pin.find(pinID).photos.each { |x|
      if x[:hot].nil?
        x.update_attributes(hot: 0)
      end
      hash = { :photoID => x[:id], :url => PHOTO_URL+x[:filedir], :score => x[:score], :hot => x[:hot], :tags => Photo.find(x[:id]).tags.split("#"),:caption => x[:caption]}
      result.push(hash)
    }
    return result
  end

  def self.vote(photoID, dv)
    photo = Photo.find(photoID)
    if Integer(dv) == 0
      photo.update_attributes(score: Photo.getScore(photoID) + 1)
      pin = Pin.find(photo.pin[:id])
      pin.update_attributes(score: Pin.getScore(photo.pin) + 1)
      photo.update_attributes(hot: Photo.hot(photo.score, photo.created_at))
      return photo.score
    elsif Integer(dv) == 1
      photo.update_attributes(score: Photo.getScore(photoID) - 1)
      pin = Pin.find(photo.pin[:id])
      pin.update_attributes(score: Pin.getScore(photo.pin) - 1)
      photo.update_attributes(hot: Photo.hot(photo.score, photo.created_at))
      return photo.score
    end
  end

  def self.hot(score, date)
    order = Math.log10([1, score.abs].max)
    if score > 0
      sign = 1
    elsif score < 0
      sign = -1
    else 
      sign = 0
    end
    seconds = Photo.eseconds(date) - 1134028003
    return (order + sign * seconds / 45000).round(4)
  end

  def self.eseconds(date)
    timeDiff = date - DateTime.new(1970, 1, 1)
    return timeDiff::days * 86400 + timeDiff::seconds
  end
  # def self.downvote(photoID)
  #   photo = Photo.find(photoID)
  #   if photo != nil
  #     photo.update_attributes(score: Photo.getScore(photoID) - 1)
  #     pin = Pin.find(photo.pin[:id])
  #     pin.update_attributes(score: Pin.getScore(photo.pin) - 1)
  #     return photo.score
  #   end
  # end

  def self.getScore(photoID)
    return Photo.find(photoID)::score
  end

  def self.getHot(photoID)
    return Photo.find(photoID)::hot
  end

  def self.getCaption(photoID)
    return Photo.find(photoID)::caption
  end

  def self.TESTAPI_resetFixture()
    self.delete_all
    return SUCCESS
  end
end