"""
Each file that starts with test... in this directory is scanned for subclasses of unittest.TestCase or testLib.RestTestCase
"""

import unittest
import os
import testLib
import json

class TestUserAdd(testLib.RestTestCase):
    """
    Test Server Returning Error Message
    """
    def testAddEmptyUser(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'username' : '', 'password' : 'qqqqqq', 'count' : 0 })
        self.assertDictEqual(respData, {'errCode' : testLib.RestTestCase.ERR_BAD_USERNAME, 'cookies' : {} })
    def testAddLongUser(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'username' : 'a' * 21, 'password' : 'qqqqqq', 'count' : 0 })
        self.assertDictEqual(respData, {'errCode' : testLib.RestTestCase.ERR_BAD_USERNAME,'cookies' : {}  })
    def testAddSameUser(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'ihatetests', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.makeRequest("/users/add", method="POST", data = {'username' : 'ihatetests', 'password' : 'qqqqqq', 'count' : 0 })
        self.assertDictEqual(respData, {'errCode' : testLib.RestTestCase.ERR_USER_EXISTS,'cookies' : {}  })
    def testAddShortPassword(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'username' : 'james', 'password' : 'aa', 'count' : 0 })
        self.assertDictEqual(respData, {'errCode' : testLib.RestTestCase.ERR_BAD_PASSWORD,'cookies' : {}  })

    """
    Test Server Sending Cookies (Adding user creates cookies on the client side)
    """
    def testAddDifferentUser1(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'username' : 'u1', 'password' : 'pppppp'})
        self.assertDictEqual(respData, {'errCode' : testLib.RestTestCase.SUCCESS, 'count' : 1, 'cookies' : {'session_id' : 'u1'}  })
    def testAddDifferentUser2(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'u1', 'password' : 'pppppp'})
        self.makeRequest("/users/add", method="POST", data = {'username' : 'u2', 'password' : 'pppppp'})
        respData = self.makeRequest("/users/add", method="POST", data = {'username' : 'u3', 'password' : 'pppppp'})
        self.assertDictEqual(respData, {'errCode' : testLib.RestTestCase.SUCCESS, 'count' : 1, 'cookies' : {'session_id' : 'u3'}  })

 
class TestUserLogin(testLib.RestTestCase):
    """
    Test Server Returning Error Message
    """
    def testLoginInvalidUsef(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 1 })
        respData1 = self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqd', 'count' : 2 })
        self.assertDictEqual(respData1, { 'errCode' : testLib.RestTestCase.ERR_BAD_CREDENTIALS, 'cookies' :{} })
        self.makeRequest("/users/login", method="POST", data = {'username' : 'beaco', 'password' : 'qqqqqq', 'count' : 1 })
        respData2 = self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqd', 'count' : 2 })
        self.assertDictEqual(respData2, { 'errCode' : testLib.RestTestCase.ERR_BAD_CREDENTIALS, 'cookies' : {} })

    """
    Test Server Sending Cookies (Login user should create cookies on the client side)
    """  
    def testLoginValidUser(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 1 })
        respData = self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 2 })
        self.assertDictEqual(respData, { 'errCode' : testLib.RestTestCase.SUCCESS, 'count' : 3, 'cookies' : {'session_id' : 'beacon'} })
    def testLoginValidUser2(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 1 })
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon2', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.makeRequest("/users/login", method="POST", data = {'username' : 'beacon2', 'password' : 'qqqqqq', 'count' : 2 })
        self.assertDictEqual(respData, { 'errCode' : testLib.RestTestCase.SUCCESS, 'count' : 2, 'cookies' : {'session_id' : 'beacon2'} })
    
class TestUserAddEmail(testLib.RestTestCase):
    """
    Test Adding Email
    """  
    def testAddEmail(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.makeRequest("/users/addEmail", method="POST", data = {'email' : 'alberthuang@berkeley.edu'}, thecookies = {'session_id' : 'beacon'})
        self.assertDictEqual(respData, {"errCode":1,"email":"alberthuang@berkeley.edu","session_id":"beacon", 'cookies' : {}})
    
    """
    Test Adding Email, By Using Cookie Sent By The Server Before
    """  
    def testAddEmailUsingCookieReceived(self):
        resp = self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.makeRequest("/users/addEmail", method="POST", data = {'email' : 'alberthuang@berkeley.edu'}, thecookies = resp['cookies'])
        self.assertDictEqual(respData, {"errCode":1,"email":"alberthuang@berkeley.edu","session_id":"beacon", 'cookies' : {}})


class TestUserUpload(testLib.RestTestCase):
    """
    Test Upload Photo to Server
    """  
    def testUpLoadPhoto(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        self.assertEquals(respData["session_id"], "beacon")
        self.assertTrue("Test.png" in respData["message"])
        self.assertEquals("this is a great caption", respData["caption"])

    """
    Test Upload Photo to Server, By Using Cookie Sent By The Server Before
    """  
    def testUpLoadPhotoUsingCookieReceived(self):
        resp = self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 }) 
        respData = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = resp['cookies'])
        self.assertEquals(respData["session_id"], "beacon")
        self.assertTrue("Test.png" in respData["message"])
        self.assertEquals("this is a great caption", respData["caption"])


class TestGetPin(testLib.RestTestCase):
    """
    Test Adding a Picture Will Create a Pin
    """  
    def testGetOnePin(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        resp = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : -133, 'lon' : 20, 'rad' : 1 })
        self.assertEquals(resp['pins'][0]['longitude'], 20)
        self.assertEquals(resp['pins'][0]['latitude'], -133)

    """
    Test Getting Pin Returns Only Pins in the Range
    """  
    def testGetPinsOnlyInRadius(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        resp = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : -133, 'lon' : 20, 'rad' : 1 })
        self.assertEquals(len(resp['pins']), 0)
        resp = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : -133, 'lon' : 19, 'rad' : 1 })
        self.assertEquals(len(resp['pins']), 1)
        self.assertEquals(resp['pins'][0]['longitude'], 19)
        self.assertEquals(resp['pins'][0]['latitude'], -133)

    """
    Test Getting Multiple Pins
    """  
    def testGetMultiplePins(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19.01, 'latitude' :-133.01, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        resp = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : -133, 'lon' : 19, 'rad' : 10 })
        self.assertEquals(len(resp['pins']), 2)

    """
    Test Aggregating Pins
    """  
    def testGetPinsAggregated(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19.00001, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        resp = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : -133, 'lon' : 19, 'rad' : 10 })
        self.assertEquals(len(resp['pins']), 1)
        

class TestGetPhoto(testLib.RestTestCase):
    """
    Test Getting the Photo URL
    """  
    def testGetOnePhoto(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        resp = self.makeRequest("/photos/getPhotos", method="GET", data = {'pinID':upload["pinID"]}, thecookies={})
        #Caution here. Our server sometimes returns a list and sometimes retrun a dictionary. Here it returns a list.
        self.assertTrue('http://s3.amazonaws.com/zhilinghuangbeacon/' in resp[0]['url'])
        self.assertTrue('Test.png' in resp[0]['url'])
        self.assertEquals(photoID,resp[0]['photoID'])
        self.assertEquals("this is a great caption",resp[0]['caption'])

    """
    Test Getting Multiple Photo URL, After Aggregating
    """  
    def testGetMultiplePhotos(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        upload2 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19.0001, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID2 = upload2["photoID"]
        resp = self.makeRequest("/photos/getPhotos", method="GET", data = {'pinID':upload["pinID"]}, thecookies={})
        #Caution here. Our server sometimes returns a list and sometimes retrun a dictionary. Here it returns a list.
        self.assertTrue('http://s3.amazonaws.com/zhilinghuangbeacon/' in resp[0]['url'])
        self.assertTrue('Test.png' in resp[0]['url'])
        self.assertTrue('http://s3.amazonaws.com/zhilinghuangbeacon/' in resp[1]['url'])
        self.assertTrue('Test.png' in resp[1]['url'])
        self.assertTrue((photoID == resp[0]['photoID'] and photoID2 == resp[1]['photoID']) or (photoID == resp[1]['photoID'] and photoID2 == resp[0]['photoID']))




class TestVoteAndHot(testLib.RestTestCase):
    """
    Test Basic Get Score
    """  
    def testBasicGetScore(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        resp = self.makeRequest("/photos/getScore", method="GET", data = {'photoID' : photoID}, thecookies={})
        self.assertEquals(resp['score'], 0)

    """
    Test UpVoting
    """  
    def testVote(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        resp = self.makeRequest("/photos/getScore", method="GET", data = {'photoID' : photoID}, thecookies={})
        resp2 = self.makeRequest("/photos/getHot", method="GET", data = {'photoID' : photoID}, thecookies={})
        self.assertEquals(resp['score'], 4)
        self.assertNotEquals(0, resp2['hot'])

    """
    Test DownVoting
    """  
    def testDownVote(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        resp = self.makeRequest("/photos/getScore", method="GET", data = {'photoID' : photoID}, thecookies={})
        resp2 = self.makeRequest("/photos/getHot", method="GET", data = {'photoID' : photoID}, thecookies={})
        self.assertEquals(resp['score'], -4)
        self.assertNotEquals(0, resp2['hot'])


    """
    Test Up And DownVoting
    """  
    def testUpAndDownVote(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 1}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        resp = self.makeRequest("/photos/getScore", method="GET", data = {'photoID' : photoID}, thecookies={})
        resp2 = self.makeRequest("/photos/getHot", method="GET", data = {'photoID' : photoID}, thecookies={})
        self.assertEquals(resp['score'], 2)
        self.assertNotEquals(0, resp2['hot'])



class TestGetCaption(testLib.RestTestCase):
    """
    Test Get Caption
    """  
    def testGetCaption1(self):
        caption = "this is a great caption"
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : caption , 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = respData["photoID"]
        resp = self.makeRequest("/photos/getCaption", method="GET", data = {'photoID' : photoID}, thecookies={})
        self.assertEquals(caption, resp["caption"])


    """
    Test Get Caption
    """  
    def testGetCaption2(self):
        caption = "this is a great caption"
        caption2 = "this is a bad caption"
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        respData = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : caption , 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 20, 'latitude' :-133, 'caption' : caption2 , 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = respData["photoID"]
        resp = self.makeRequest("/photos/getCaption", method="GET", data = {'photoID' : photoID}, thecookies={})
        self.assertEquals(caption, resp["caption"])

class TestGetPinScore(testLib.RestTestCase):
    """
    Test Get Score of Pin With Single Photo
    """  
    def testGetScoreofPinWithSinglePhoto(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 19, 'latitude' :-133, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        pinID = upload["pinID"]
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        resp = self.makeRequest("/pins/getScore", method="GET", data = {'pinID' :pinID}, thecookies={})
        self.assertEquals(2, resp["score"])


    """
    Test Get Score of Pin With Multiple Photo
    """  
    def testGetScoreofPinWithMultiplePhotos(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 122.2728, 'latitude' :37.8717, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        upload2 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 122.27279, 'latitude' :37.8717, 'caption' : "this is a great caption", 'tags' : ""}, thecookies = {'session_id' : 'beacon'})
        photoID = upload["photoID"]
        photoID2 = upload2["photoID"]
        pinID = upload["pinID"]   # They should have the same pinID
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID2, 'dv' : 0}, thecookies={})
        self.makeRequest("/photos/vote", method="POST", data = {'photoID' : photoID2, 'dv' : 0}, thecookies={})
        resp = self.makeRequest("/pins/getScore", method="GET", data = {'pinID' :pinID}, thecookies={})
        self.assertEquals(4, resp["score"])


class TestTags(testLib.RestTestCase):
    def testNotSimilarButCloseTagsAggregate(self):
        #Photos aggregates into one pin because location very close, even though tags dont contain
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload1 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30, 'caption' : "this is a great caption", 'tags' : 'Not#Contains#Tags#'}, thecookies = {'session_id' : 'beacon'})
        upload2 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30.0001, 'caption' : "this is a great caption", 'tags' : 'Tags#Contains#TestContains#'}, thecookies = {'session_id' : 'beacon'})
        self.assertEquals(upload1['pinID'], upload2['pinID'])
        #The aggregated pin only has the union of two tags
        thePin = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : 30, 'lon' : 30, 'rad' : 1 })['pins'][0]
        self.assertEquals(thePin['tags'], ['Not','Contains', 'Tags', 'TestContains'])
        #Even though aggregated, the photo tags remain the same as user input
        self.assertEquals(upload1['tags'], 'Not#Contains#Tags#')
        self.assertEquals(upload2['tags'], 'Tags#Contains#TestContains#')

    def testNotSimilarAndFarTagsDontAggregate(self):
        #Photos do not aggregate into one pin because tags do not contain one way or the other
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload1 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30, 'caption' : "this is a great caption", 'tags' : 'Not#Contains#Tags#'}, thecookies = {'session_id' : 'beacon'})
        upload2 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30.0002, 'caption' : "this is a great caption", 'tags' : 'Tags#Contains#TestContains#'}, thecookies = {'session_id' : 'beacon'})
        self.assertEquals(upload1['pinID']==upload2['pinID'],False)
        #Even though aggregated, the photo tags remain the same as user input
        self.assertEquals(upload1['tags'], 'Not#Contains#Tags#')
        self.assertEquals(upload2['tags'], 'Tags#Contains#TestContains#')
    def testSimilarAlthoughFarTagsAggregate(self):
        #Photos aggregate into one pin because tags have a contain relation
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload1 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30, 'caption' : "this is a great caption", 'tags' : 'Contains#Tags#'}, thecookies = {'session_id' : 'beacon'})
        upload2 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30.0002, 'caption' : "this is a great caption", 'tags' : 'Tags#Contains#TestContains#'}, thecookies = {'session_id' : 'beacon'})
        self.assertEquals(upload1['pinID'], upload2['pinID'])
        #The aggregated pin only has the union of two tags
        thePin = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : 30, 'lon' : 30, 'rad' : 1 })['pins'][0]
        self.assertEquals(thePin['tags'], ['Contains', 'Tags', 'TestContains'])
        #Even though aggregated, the photo tags remain the same as user input
        self.assertEquals(upload1['tags'], 'Contains#Tags#')
        self.assertEquals(upload2['tags'], 'Tags#Contains#TestContains#')
    def testGetPhotosTags(self):
        self.makeRequest("/users/add", method="POST", data = {'username' : 'beacon', 'password' : 'qqqqqq', 'count' : 0 })
        upload1 = self.sendFileRequest("/users/upload",file="Test.png", data = {'longitude' : 30, 'latitude' :30, 'caption' : "this is a great caption", 'tags' : 'Contains#Tags#'}, thecookies = {'session_id' : 'beacon'})
        thePin = self.makeRequest("/pins/getPins", method="GET", data = {'lat' : 30, 'lon' : 30, 'rad' : 1 })['pins'][0]
        #Test pin's tags
        self.assertEquals(thePin['tags'], ['Contains', 'Tags'])
        tags = self.makeRequest("/photos/getPhotos", method="GET", data = {'pinID':upload1["pinID"]}, thecookies={})[0]['tags']
        self.assertEquals(tags, ['Contains','Tags'])
        








   






