require 'test_helper'

class UsersmodelTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  #unit test 1
  test "add user" do
    Usersmodel.TESTAPI_resetFixture()
    result = Usersmodel.add({:user => "u1", :password => "p1"})
    assert(result==Usersmodel::SUCCESS)
  end

  #unit test 2
  test "add user exists" do
    Usersmodel.TESTAPI_resetFixture()
    Usersmodel.add({:user => "u1", :password => "p1"})
    result = Usersmodel.add({:user => "u1", :password => "p1"})
    assert(result==-2)
  end
  #unit test 3
  test "add user bad username empty" do
    Usersmodel.TESTAPI_resetFixture()
    result = Usersmodel.add({:user => "", :password => "p1"})
    assert(result==-3)
  end

  #unit test 4
  test "add user bad username too long" do
    Usersmodel.TESTAPI_resetFixture()
    result = Usersmodel.add({:user => "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", :password => "p1"})
    assert(result==-3)
  end

  #unit test 5
  test "add user bad password too long" do
    Usersmodel.TESTAPI_resetFixture()
    result = Usersmodel.add({:password => "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", :user => "u1"})
    assert(result==-4)
  end

  #unit test 6
  test "log in" do
    Usersmodel.TESTAPI_resetFixture()
    Usersmodel.add({:user => "u1", :password => "p1"})
    result = Usersmodel.login({:user => "u1", :password => "p1"})
    assert(result==2)
  end

  #unit test 7
  test "log in bad credential (1)" do
    Usersmodel.TESTAPI_resetFixture()
    Usersmodel.add({:user => "u1", :password => "p1"})
    result = Usersmodel.login({:user => "u1", :password => "p11234"})
    assert(result==-1)
  end

  #unit test 8
  test "log in bad credential (2)" do
    Usersmodel.TESTAPI_resetFixture()
    Usersmodel.add({:user => "u1", :password => "p1"})
    result = Usersmodel.login({:user => "u1234", :password => "p1"})
    assert(result==-1)
  end

  #unit test 9
  test "log in bad credential case sensitive(1)" do
    Usersmodel.TESTAPI_resetFixture()
    Usersmodel.add({:user => "u1", :password => "p1"})
    result = Usersmodel.login({:user => "u1", :password => "P1"})
    assert(result==-1)
  end

  #unit test 10
  test "log in bad credential case sensitive(2)" do
    Usersmodel.TESTAPI_resetFixture()
    Usersmodel.add({:user => "u1", :password => "p1"})
    result = Usersmodel.login({:user => "U1", :password => "p1"})
    assert(result==-1)
  end

  #unit test 11
  test "log in counter increments (1)" do
    Usersmodel.TESTAPI_resetFixture()
    # log in in total 7 times
    Usersmodel.add({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    result = Usersmodel.login({:user => "u1", :password => "p1"})
    assert(result==7)
  end

  #unit test 12
  test "log in counter increments (2)" do
    Usersmodel.TESTAPI_resetFixture()
    # log in in total 7 times
    Usersmodel.add({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})
    Usersmodel.login({:user => "u1", :password => "p1"})

    # and other users log in doesn't matter
    Usersmodel.add({:user => "u2", :password => "p2"})
    Usersmodel.login({:user => "u2", :password => "p2"})

    result = Usersmodel.login({:user => "u1", :password => "p1"})
    assert(result==7)
  end

  #unit test 13
  test "delete database" do
    Usersmodel.TESTAPI_resetFixture()

    Usersmodel.add({:user => "u1", :password => "p1"})
    Usersmodel.add({:user => "u2", :password => "p2"})
    Usersmodel.add({:user => "u3", :password => "p3"})
    Usersmodel.add({:user => "u4", :password => "p4"})
    Usersmodel.TESTAPI_resetFixture()

    result = Usersmodel.count
    assert(result==0)
  end



end
