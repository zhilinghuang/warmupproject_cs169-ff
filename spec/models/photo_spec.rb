require 'rails_helper'

describe Photo do
  before :each do
    user_params = { username: 'james', password: 'jameskim', count: 1}
    @check = User.add(user_params)
    @james = User.find_by_username('james')
    @jamesID = @james[:id]
    user_params = { username: 'zhiling', password: 'wojiushiniu', count: 1}
    User.add(user_params)
    @zhiling = User.find_by_username('zhiling')
    @zhilingID = @zhiling[:id]
  end

  describe "validations" do
    it "add photo" do
      aPhoto = Photo.add('zhiling', 'Test.png', 20, 20, File.open("Test.png", 'r'), 'great caption','')
      expect(aPhoto).to be_instance_of(Photo)
    end
  end

  describe "validations" do
    it "add photos, will aggregate" do
      Photo.add('zhiling', 'Test.png', 20, 20, File.open("Test.png", 'r'), 'great caption','')
      Photo.add('zhiling', 'Test.png', 20, 20.00000000001, File.open("Test.png", 'r'), 'great caption','')
      #Only one pin
      expect(Pin.getPins(20,20,1).size()).to eq(1)
      #With two pictures
      expect(Photo.getPhotos(Pin.getPins(20,20,1)[0][:id]).size()).to eq(2)
    end
  end

  describe "validations" do
    it "vote, and getscore" do
      aPhoto = Photo.add('zhiling', 'Test.png', 20, 20, File.open("Test.png", 'r'), 'great caption','')
      bPhoto = Photo.add('james', 'Test2.png', 20, 20, File.open("Test2.png", 'r'), 'great caption1','')
      Photo.vote(aPhoto[:id], 0)
      Photo.vote(aPhoto[:id], 0)
      Photo.vote(aPhoto[:id], 0)
      Photo.vote(aPhoto[:id], 1)
      Photo.vote(aPhoto[:id], 1)
      Photo.vote(aPhoto[:id], 1)
      Photo.vote(aPhoto[:id], 0)
      Photo.vote(bPhoto[:id], 0)
      expect(Photo.getScore(aPhoto[:id])).to eq(1)
      expect(Photo.getHot(aPhoto[:id])).not_to eq(Photo.getHot(bPhoto[:id]))
    end
  end

  describe "validations" do
    it "add photos, will aggregate" do
      Photo.add('zhiling', 'Test.png', 20, 20, File.open("Test.png", 'r'), 'great caption','')
      Photo.add('zhiling', 'Test.png', 20, 20.00000000001, File.open("Test.png", 'r'), 'great caption','')
      Photo.TESTAPI_resetFixture()
      expect(Photo.all.size()).to eq(0)
    end
  end

  describe "validations" do
    it "test tags" do
      aPhoto = Photo.add('zhiling', 'Test.png', 20, 20, File.open("Test.png", 'r'), 'great caption','pizza#is#good#')
      expect(aPhoto.tags).to eq('pizza#is#good#')
    end
  end

  describe "validations" do
    it "add photos and adding tags" do
      #Photos aggregates into one pin because location very close, even though tags dont contain
      p1 = Photo.add('zhiling', 'Test.png', 30, 30, File.open("Test.png", 'r'), 'great caption','Not#Contains#Tags#')
      p2 = Photo.add('zhiling', 'Test.png', 30, 30.0001, File.open("Test.png", 'r'), 'great caption','Tags#Contains#TestContains#')
      expect(Pin.getPins(30,30,1).size()).to eq(1)
      expect(p1.pin).to eq(p2.pin)
      #The aggregated pin only has the union of two tags
      assert_equal( 'Not#Contains#Tags#TestContains#', Pin.find(p1.pin).tags)
      #Even though aggregated, the photo tags remain the same as user input
      assert_equal( 'Not#Contains#Tags#', p1.tags)
      assert_equal( 'Tags#Contains#TestContains#', p2.tags)


      #Photos do not aggregate into one pin because tags do not contain one way or the other
      p3 = Photo.add('zhiling', 'Test.png', 40, 40, File.open("Test.png", 'r'), 'great caption','Not#Contains#Tags#')
      p4 = Photo.add('zhiling', 'Test.png', 40, 40.0002, File.open("Test.png", 'r'), 'great caption','Tags#Contains#TestContains#')
      expect(Pin.getPins(40,40,1).size()).to eq(2)
      assert_equal( 'Not#Contains#Tags#', p3.tags)
      assert_equal( 'Tags#Contains#TestContains#', p4.tags)

      #Photos aggregate into one pin because tags contain each other
      p5 = Photo.add('zhiling', 'Test.png', 20, 20, File.open("Test.png", 'r'), 'great caption','Contains#Tags#')
      p6 = Photo.add('zhiling', 'Test.png', 20, 20.0002, File.open("Test.png", 'r'), 'great caption','Tags#Contains#TestContains#')
      expect(Pin.getPins(20,20,1).size()).to eq(1)
      expect(p5.pin).to eq(p6.pin)
      #The aggregated pin only has the union of two tags
      assert_equal( 'Contains#Tags#TestContains#', Pin.find(p5.pin).tags)
      #Even though aggregated, the photo tags remain the same as user input
      assert_equal( 'Contains#Tags#', p5.tags)
      assert_equal( 'Tags#Contains#TestContains#', p6.tags)

    end
  end

end


