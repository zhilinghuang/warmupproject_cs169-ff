require 'rails_helper'

describe User do
	before :each do
		user_params = { username: 'james', password: 'jameskim', count: 1}
		@check = User.add(user_params)
		@james = User.find_by_username('james')
    @jamesID = @james[:id]
    user_params = { username: 'zhiling', password: 'wojiushiniu', count: 1}
    User.add(user_params)
    @zhiling = User.find_by_username('zhiling')
    @zhilingID = @zhiling[:id]
	end

	describe "validations" do
		it "successfully adds username and password to the database" do
			expect(@james).to be_instance_of(User)
			expect(@check).to eql 1
		end
	end

	describe "validations" do
		it "does not take in string with length of less than 6 as passwords" do
			omfg_params = { username: 'wth', password: '', count: 0}
			omfg = User.add(omfg_params)
			omg_params = { username: 'wth', password: '5', count: 0}
			omg = User.add(omg_params)
			omhfg_params = { username: 'wth', password: '5' * 6, count: 0}
			omhfg = User.add(omhfg_params)
			expect(omfg).to eql -4
			expect(omg).to eql -4
			expect(omhfg).to eql 1
		end
	end


	describe "validations" do
		it "should enforce user to have string of maximum 20 characters as the username" do
			user_params = { username: 'a' * 21, password: 'a' * 7 , count: 0}
			user = User.add(user_params)
			expect(user).to eql -3
			user1_params = { username: 'a' * 20, password: 'a' * 7 , count: 0}
			user1 = User.add(user1_params)
			expect(user1).to eql 1
		end
  end

  describe "validations" do
    it "should enforce username input" do
      user_params = { username: '', password: 'doesntreallymatter' , count: 0}
      user = User.add(user_params)
      expect(user).to eql -3

    end
  end


	describe "validations" do
		it "should enforce user to have a unique username" do
			skt_params = { username: 'skt', password: 'a' * 7 , count: 0}
			User.add(skt_params)
			fnatic_params = { username: 'skt', password: 'b' * 7 , count: 0}
			fnatic = User.add(fnatic_params)
			expect(fnatic).to eql -2
		end
	end

	describe "validations" do
		it "shouldn't let us log in with a unregistered user" do
			lmq_params = { username: 'qwerzxcv', password: 'asdfasd', count: 0 }
			lmq = User.login(lmq_params)
			expect(lmq).to eql -1
		end
  end

  describe "validations" do
    it "shouldn't let us log in with no username" do
      lmq_params = { password: 'asdfasd', count: 0 }
      lmq = User.login(lmq_params)
      expect(lmq).to eql -1
    end
  end
	
	describe "validations" do
		it "shouldn't let us log in with wrong password" do
			najin_params = { username: 'james', password: 'kimjames', count: 0 }
			najin = User.login(najin_params)
			expect(najin).to eql -1
		end
  end

  describe "validations" do
    it "testing adding email" do
      lmq_params = { username: 'zhiling', email: 'alberthuang@berkeley.edu'}
      lmq = User.addEmail(lmq_params)
      expect(lmq).to eql 1
    end
  end

  describe "validations" do
    it "testing adding email, not using any username" do
      lmq_params = {email: 'alberthuang@berkeley.edu'}
      lmq = User.addEmail(lmq_params)
      expect(lmq).to eql -1
    end
  end

  describe "validations" do
    it "testing adding replicate email," do
      lmq_params = {username: 'zhiling', email: 'alberthuang@berkeley.edu'}
      lmq = User.addEmail(lmq_params)
      mq_params = {username: 'james', email: 'alberthuang@berkeley.edu'}
      lmq = User.addEmail(lmq_params)
      expect(lmq).to eql -5
    end
  end

  describe "validations" do
    it "testing getID" do
      expect(User.getID('zhiling')).to eql @zhilingID
      expect(User.getID('james')).to eql @jamesID
    end
  end

  describe "validations" do
    it "testing clear db" do
      User.TESTAPI_resetFixture()
      user_params = { username: 'james', password: 'jameskim', count: 1}
      expect(User.add(user_params)).to eql 1
    end
  end


  # describe "validations" do
  #   it "testing adding email, with no username" do
  #     lmq_params = {password: 'thatsright', count: 0 }
  #     lmq = User.addEmail({email: 'soniubi@niubi.com'})
  #     expect(lmq).to eql -1
  #   end
  # end



  # self.getID(username)
  # self.TESTAPI_resetFixture()

end