require 'rails_helper'

describe Pin do
	before :each do
		@pin1ID = Pin.add(90, 180, 0,'')
		@pin1 = Pin.find(@pin1ID)
    @pin2ID = Pin.add(90, 180.000000001, 0,'')
    @pin2 = Pin.find(@pin2ID)
	end
	describe "validations" do
		it "successfully adds pin to the database, with correct information" do
			expect(@pin1).to be_instance_of(Pin)
      expect(Pin.find(@pin1ID)).to be_instance_of(Pin)
      expect(Pin.getScore(@pin1ID)).to eql(0)
		end
  end
  describe "validations" do
    it "successfully clears table" do
      Pin.TESTAPI_resetFixture()
      expect(Pin.all.length).to eql(0)
    end
  end
  describe "validations" do
    it "successfully get all pins in the radius" do
      expect(Pin.getPins(90,100,999999).size()).to eql(2)
      pin3ID = Pin.add(90, 180.000000002, 0,'')
      pin3 = Pin.find(pin3ID)
      expect(Pin.getPins(90,100,999999).size()).to eql(3)
    end
  end
  describe "validations" do
    it "add tags" do
      pin3ID = Pin.add(200, 200, 0,'#this#pin#smells#good')
      expect(Pin.find(pin3ID).tags).to eql('#this#pin#smells#good')
    end
  end
end